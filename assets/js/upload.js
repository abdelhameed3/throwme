

/* share */
function copyToClipboard(element) {
    $('.copyToClipboard').slideDown();
    $('.userProfile .green').slideDown();
    
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
}
/* Add active */
$(document).ready(function(){
    $(".price").change(function (){
        if($("input#customRadio2").is(':checked')){
            $(".form-group.som").addClass("somshow");
        }else {
            $(".form-group.som").removeClass("somshow");
        }
    });

   $(".fixedItem .icon").click(function(e){
       $(".fixedItem").toggleClass("active");
   });
 
    $(".card-body .cardSetting i.fa-trash").click(function(e){
         e.stopPropagation();
       $(this).next(".confirmRemove").toggleClass("active");
   });
    // Remove item
    $(".confirmRemove .btn").click(function (e) {
        e.preventDefault();
        $(this).closest(".card").fadeOut();
    });
    $('body').click(function(){
        if( $(".card-body .cardSetting .confirmRemove").hasClass("active")  ){
              $(".card-body .cardSetting .confirmRemove").removeClass("active");
            };
        });
});
 /* Slider */
$(document).ready(function () {
    
    'use strict';
    $(".add-details .slider").slick({
            prevArrow: "<button type='button' class='slick-next pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
            nextArrow: "<button type='button' class='slick-prev pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
            speed: 300,
            infinite: true,
            autoplay: false,
            draggable:false,
            rtl: true
        });
    $('.card-columns .card .slider,.row .card .slider').each(function(index, element) {
        var slider = $(this);
        var next = slider.find(".arrows span.next"),
            prev = slider.find(".arrows span.prev");
            //init slider
        
            slider.slick({
            prevArrow: "<button type='button' class='slick-next pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
            nextArrow: "<button type='button' class='slick-prev pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
            speed: 300,
            infinite: true,
            autoplay: false,
            draggable:false,
            rtl: true
        });
    });
   
});
//=============================== Upload image ======================= //

var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};var compose = function compose() {for (var _len = arguments.length, fns = Array(_len), _key = 0; _key < _len; _key++) {fns[_key] = arguments[_key];};return fns.reduce(function (f, g) {return function () {return f(g.apply(undefined, arguments));};});};

var validateFileSize = function validateFileSize(params) {
//  console.log(params.file.size / 1024 / 1024);
  if (!params.hasError && params.file.size / 1024 / 1024 > 4) {
    return _extends({},
    params, {
      hasError: true,
      errorMessage: "تم تجاوز الحد الأقصى لحجم الملف. الحد الأقصى للحجم هو 4 ميغابايت." });

  }
  return params;
};

var validateFileType = function validateFileType(params) {
  var types = ["jpeg", "jpg", "png", "webp", "gif", "bmp"],acceptedTypes = new RegExp(types.join("|"));

  if (!params.hasError && !acceptedTypes.test(params.file.type)) {
    return _extends({},
    params, {
      hasError: true,
      errorMessage: "نوع الملف غير صحيح. يرجى محاولة واحدة من التالي: " + types.join(',') });

  }
  return params;
};

var readFile = function readFile(params) {
  if (!params.hasError) {
    var reader = new FileReader();

    reader.onload = function (e) {
      var img = new Image();
      img.src = e.target.result;
      params.destination.appendChild(img);
    };
    reader.readAsDataURL(params.file);
  }
  return params;
};

var handleError = function handleError(params) {
  if (params.hasError) {
    params.errorContainer.innerHTML = params.errorMessage;
  }
  return params;
};
function handleChange(e) {
  var errorContainer = document.querySelector('#error'),
      files = e.target.files,
      destination = document.querySelector('#results');
  errorContainer.innerHTML = '';
  destination.innerHTML = '';

  Object.keys(files).map(function (i) {
    // let file = files[i];
    var params = {
      file: files[i],
      hasError: false,
      errorMessage: '',
      destination: destination,
      errorContainer: errorContainer };


    var validateFile = compose(
    readFile,
    handleError,
    validateFileType,
    validateFileSize);


    validateFile(params);
  });
}

/* Upload image */
$(function () {

  // Viewing Uploaded Picture On Setup Admin Profile
  function livePreviewPicture(picture)
  {
    if (picture.files && picture.files[0]) {
      var picture_reader = new FileReader();
      picture_reader.onload = function(event) {
        $('#uploaded').attr('src', event.target.result);
      };
      picture_reader.readAsDataURL(picture.files[0]);
    }
  }

  $('.logo-admin input').on('change', function () {
    $('#uploaded').fadeIn();
    livePreviewPicture(this);
  });

});
$(document).ready(function () {
    
    'use strict';
    $(".ti-layout-column3").click(function (){
        if($(".defualt").hasClass("showHorz")){
            $(".slick-next").click();
        }
       $(".defualt").removeClass("showHorz"); 
        $(".defualt").parent().addClass("col-lg-4").removeClass("col-lg-12");
        

    });
    $(".ti-view-list").click(function (){
        if(!$(".defualt").hasClass("showHorz")){
            $(".slick-next").click();
        }
       $(".defualt").addClass("showHorz");
        $(".defualt").parent().removeClass("col-lg-4").addClass("col-lg-12");
        

    });
});
 // Get the modal
var modal = document.getElementById('ShowImg');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
$(".showProtuct .img-fluid").click(function (){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}); 


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}
$(document).ready(function () {
    
    'use strict';
    $(".readP-more").click(function (){
        $(this).hide();
        $(".p-more").addClass("autoH");
    })
});
$('.datepicker').datepicker({
        weekStart: 1,
        autoclose: true,
        todayHighlight: true,
    });
    $('.datepicker').datepicker("setDate", new Date());